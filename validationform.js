$(function() {
    $("fname_error_message").hide();
    $("lname_error_message").hide();
    $("email_error_message").hide();
    $("password_error_message").hide();
    $("retypepassword_error_message").hide();

    var error_fname = false;
    var error_lname = false;
    var error_email = false;
    var error_password = false;
    var error_retypepassword = false;

    $("#fname").focusout(function() {
        check_fname();
    })
    $("#lname").focusout(function() {
        check_lname();
    })
    $("#email").focusout(function() {
        check_email();
    })
    $("#password").focusout(function() {
        check_password();
    })
    $("#retypepassword").focusout(function() {
        check_retypepassword();
    })

    function check_fname() {
        var pattern = /^[a-zA-z]*$/;
        var fname = $("fname").val()
        if (pattern.test(fname) && fname !== '') {
            $('#fname_error_message').hide();
            $('#fname').css('border-bottom', '2px solid #34F458');
        } else {
            $('#fname_error_message').html('Should contain only characters');
            $('#fname_error_message').show();
            $('#fname').css('border-bottom', '2px solid #F90A0A');
            error_fname = true;
        }
    }

    function check_lname() {
        var pattern = /^[a-zA-z]*$/;
        var lname = $("lname").val()
        if (pattern.test(lname) && lname !== '') {
            $('#lname_error_message').hide();
            $('#lname').css('border-bottom', '2px solid #34F458');
        } else {
            $('#lname_error_message').html('Should contain only characters');
            $('#lname_error_message').show();
            $('#lname').css('border-bottom', '2px solid #F90A0A');
            error_lname = true;
        }
    }

    function check_password() {
        var password_length = $('password').val().length;
        if (password_length < 8) {
            $('#password_error_message').html('Atleast 8 characters');
            $('#passowrd_error_message').show();
            $('#password').css('border-bottom', '2px solid #F90A0A');
            error_password = true;
        } else {
            $('#password_error_message').hide();
            $('#password').css('border-bottom', '2px solid #34F458');
        }
    }

    function check_retypepassword() {
        var password = $('#password').val();
        var retypepassowrd = $('#retypepassword').val();
        if (password !== retypepassowrd) {
            $('#retypepassword_error_message').html('Password did not match');
            $('#retypepassowrd_error_message').show();
            $('#retypepassword').css('border-bottom', '2px solid #F90A0A');
            error_retypepassword = true;
        } else {
            $('#retypepassword_error_message').hide();
            $('#retypepassword').css('border-bottom', '2px solid #34F458');
        }
    }

    function check_email() {
        var pattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var email = $('#email').val();
        if (pattern.test(email) && email !== '') {
            $('#email_error_message').hide();
            $('#email').css('border-bottom', '2px solid #34F458');
        } else {
            $('#email_error_message').html('Invalid Email');
            $('#email_error_message').show();
            $('#email').css('border-bottom', '2px solid #F90A0A');
            error_email = true;
        }
    }
    $('#register').submit(function() {
        error_fname = false;
        error_lname = false;
        error_email = false;
        error_password = false;
        error_retypepassword = false;

        check_fname();
        check_lname();
        check_email();
        check_password();
        check_retypepassword();

        if (error_fname === false && error_lname === false && error_email === false && error_password === false && error_retypepassword === false) {
            alert('Registration Successfully Done!!');
            return true;
        } else {
            alert('Fill the form correctly');
            return false;
        }
    })
})